const { defineConfig } = require('@vue/cli-service')
const NodePolyfillPlugin = require('node-polyfill-webpack-plugin')
const path = require('path')
module.exports = defineConfig({
  transpileDependencies: true,
  configureWebpack: {
    plugins: [new NodePolyfillPlugin()]
  },
  chainWebpack: (config) => {
    config.resolve.alias
      .set('node_modules', path.join(__dirname, './node_modules'))
      .set('@', path.resolve('src'))
  }
})
