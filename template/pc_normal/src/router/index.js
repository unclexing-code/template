import Vue from 'vue'
/* Layout */
import Layout from '../layout'
import Router from 'vue-router'

Vue.use(Router)

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('../views/LoginView'),
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: 'index',
    hidden: true,
    meta: { title: '', icon: 'dashboard', affix: true },
    children: [
      {
        path: 'index',
        component: () => import('../views/HomeView'),
        //name: 'Index',
        meta: { title: '', icon: 'dashboard', affix: true }
      }
    ]
  },
  {
    path: 'attraction',
    component: Layout,
    redirect: 'noRedirect',
    hidden: false,
    children: [
      {
        path: '/attraction',
        component: () => import('../views/AboutView'),
        name: 'attractionList',
        meta: {
          title: '景点管理',
          keepAlive: true,
          icon: 'scenic',
          affix: true
        }
      }
    ]
  }
]

// 防止连续点击多次路由报错
let routerPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch((err) => err)
}

export default new Router({
  mode: 'history', // 去掉url中的#
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})
